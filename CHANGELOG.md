# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
[Code diff since last release][unreleased-diff] 

### Added

- ...

### Changed

- ...

### Removed

- ...


## [0.0.4][0.0.4-release] - 2024-06-02
[Code diff 0.0.3 -> 0.0.4][0.0.4-diff]
### Added
- implement global --unlock flag (issue #1)
- cli: print call params in debug mode
- cli: supply params via environment vars (issue #2)
- implement proper exceptions for all api error codes (issue #7)

### Changed

 - initialization: test_associate is expected to fail before making an qqassociation


## [0.0.3][0.0.3-release] - 2024-05-31
[Code diff 0.0.2 -> 0.0.3][0.0.3-diff]
### Added

- API-call 'generate-password'
- API-call 'create-new-group'
- API-call 'get-totp' (not sure if that is how it is supposed to work)
- API-call 'request-autotype' (not sure if that is how it is supposed to work)
- API-call 'set-login'
- API-call 'lock-database'

### Changed

- API-call 'get-logins': Correctly use optional "submitURL" parameter


## [0.0.2][0.0.2-release] - 2024-05-30
[Code diff 0.0.1 -> 0.0.2][0.0.2-diff]
### Added

- Changelog
- API-call 'get-database-groups'


## [0.0.1][0.0.1-release] - 2024-05-29
Initial release
### Added

- API-call 'associate'
- API-call 'change-public-keys'
- API-call 'get-databasehash'
- API-call 'get-logins'
- API-call 'test-associate'


[//]: # "Link definitions"
[unreleased-diff]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/compare/master...development
[0.0.4-release]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/releases/v0.0.4
[0.0.4-diff]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/compare/v0.0.3...v0.0.4
[0.0.3-release]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/releases/v0.0.3
[0.0.3-diff]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/compare/v0.0.2...v0.0.3
[0.0.2-release]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/releases/v0.0.2
[0.0.2-diff]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/compare/v0.0.1...v0.0.2
[0.0.1-release]: https://gitlab.com/FloRinke/pyKeepassXcBrowser/-/releases/v0.0.1
