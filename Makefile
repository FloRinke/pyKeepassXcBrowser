init:
	python -m pip install --upgrade pip
	python -m pip install --upgrade build
	python -m pip install --upgrade twine

#test:
#	py.test tests

build:
	python -m build

install:
	python -m pip install --upgrade .

install-editable:
	python -m pip install --editable .

publish:
	python3 -m twine upload --repository gitlab_pykpxcb dist/*

.PHONY: init build publish install install-editable
