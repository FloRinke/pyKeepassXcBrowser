# pyKeepassXcBrowser

Python 3 API client and CLI interface to keepassxc-browser api as defined by [keepassxc-protocol]


## Used technologies

- [KeepassXC][keepassxc-website]


## Implemented functionality
| apicall/message     | status  | description                                                                      |
|:--------------------|:--------|:---------------------------------------------------------------------------------|
| associate           | ✅      | Request for associating a new client with KeePassXC.                             |
| change-public-keys  | ✅      | Request for passing public keys from client to server and back.                  |
| create-new-group    | ✅      | Request for creating a new group to database.                                    |
| generate-password   | ✅      | Request for generating a password. KeePassXC's settings are used.                |
| get-database-groups | ✅      | Returns all groups from the active database.                                     |
| get-databasehash    | ✅      | Request for receiving the database hash (SHA256) of the current active database. |
| get-logins          | ✅      | Requests for receiving credentials for the current URL match.                    |
| get-totp            | ⚠       | Request for receiving the current TOTP.                                          |
| lock-database       | ✅      | Request for locking the database from client.                                    |
| passkeys-get        | ❌      | Request for Passkeys authentication.                                             |
| passkeys-register   | ❌      | Request for Passkeys credential registration.                                    |
| request-autotype    | ⚠       | Performs Global Auto-Type.                                                       |
| set-login           | ✅      | Request for adding or updating credentials to the database.                      |
| test-associate      | ✅      | Request for testing if the client has been associated with KeePassXC.            |
| database-locked     | ❌      | A signal from KeePassXC, the current active database is locked.                  |
| database-unlocked   | ❌      | A signal from KeePassXC, the current active database is unlocked.                |

## Testing

Currently there aren't any automated tests, yet. Contributions welcome.


## Logging

Both api-client and cli make extensive use of pythons logger facilities. See their docs on how to suit your needs


## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[LGPLv3](https://choosealicense.com/licenses/lgpl-3.0/)

## References

[keepassxc-website][keepassxc-website] - Website of the KeepassXC project

[keepassxc-protocol][keepassxc-protocol] - Definition of the KeepassXC Browser protocol


[//]: # "References definitions"
[keepassxc-website]: https://keepassxc.org/ "KeepassXC Website"
[keepassxc-protocol]: https://github.com/keepassxreboot/keepassxc-browser/blob/develop/keepassxc-protocol.md "Definition of the API in use"
