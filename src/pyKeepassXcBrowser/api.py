import logging
from typing import Union

# noinspection PyPackageRequirements
from nacl.encoding import Base64Encoder
# noinspection PyPackageRequirements
from nacl.public import PublicKey, PrivateKey

from .cryptconn import CryptConnection

logger = logging.getLogger(__name__)


def associate(conn: CryptConnection,
              client_privkey: PrivateKey,
              server_pubkey: PublicKey,
              nonce: bytes,
              client_pubkey: PrivateKey,
              identkey: PublicKey,
              unlock: bool = False) -> str:
    """Request for associating a new client with KeePassXC."""
    # Unencrypted message:
    # {
    #     "action": "associate",
    #     "key": "<client public key>",
    #     "idKey": "<a new identification public key>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "hash": "29234e32274a32276e25666a42",
    #     "version": "2.7.0",
    #     "success": "true",
    #     "id": "testclient",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    msg = {
        'action': 'associate',
        'key': client_pubkey.encode(Base64Encoder).decode("ascii"),
        'idKey': identkey.encode(Base64Encoder).decode("ascii"),
    }
    logger.debug("Trying to associate with database")
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response['id']


def change_public_keys(conn: CryptConnection,
                       client_id: str,
                       client_pubkey: PublicKey,
                       nonce: bytes) -> PublicKey:
    """Request for passing public keys from client to server and back."""
    # Request:
    # {
    #     "action": "change-public-keys",
    #     "publicKey": "<client public key>",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "clientID": "<clientID>"
    # }
    # Response (success):
    # {
    #     "action": "change-public-keys",
    #     "version": "2.7.0",
    #     "publicKey": "<host public key>",
    #     "success": "true"
    # }
    msg = {
        "action": "change-public-keys",
        "publicKey": client_pubkey.encode(Base64Encoder).decode("ascii"),
        "nonce": Base64Encoder.encode(nonce).decode("ascii"),
        "clientID": client_id
    }
    logger.debug("Request server pubkey for client pubkey %s with nonce %s",
                 client_pubkey.encode(Base64Encoder).decode("ascii"),
                 Base64Encoder.encode(nonce).decode("ascii"))
    response = conn.send_plain(msg)
    server_pubkey = response['publicKey']
    logger.debug("Received server pubkey %s with nonce %s",
                 server_pubkey,
                 response['nonce'])
    return PublicKey(server_pubkey, encoder=Base64Encoder)


def create_new_group(conn: CryptConnection,
                     client_privkey: PrivateKey,
                     server_pubkey: PublicKey,
                     nonce: bytes,
                     name: str,
                     unlock: bool = False) -> dict:
    """Request for creating a new group to database."""
    # Unencrypted message:
    # {
    #     "action": "create-new-group",
    #     "groupName": "<group name or path>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "name": "<group name>",
    #     "uuid": "<group UUID>"
    # }
    logger.debug("Create new group '%s'.", name)
    msg = {
        'action': 'create-new-group',
        'groupName': name,
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def generate_password(conn: CryptConnection,
                      client_privkey: PrivateKey,
                      server_pubkey: PublicKey,
                      nonce: bytes,
                      unlock: bool = False):
    """Request for generating a password. KeePassXC's settings are used."""
    # Request (no unencrypted message is needed):
    # {
    #     "action": "generate-password",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "clientID": "<clientID>",
    #     "requestID": "<request ID>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "version": "2.7.0",
    #     "password": "testclientpassword"
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    # Response message data (success, decrypted, KeePassXC 2.7.0 and later):
    # {
    #     "version": "2.7.0",
    #     "password": "thePassword",
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    logger.debug("Request password generation.")
    msg = {
        'action': 'generate-password'
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def get_database_groups(conn: CryptConnection,
                        client_privkey: PrivateKey,
                        server_pubkey: PublicKey,
                        nonce: bytes,
                        unlock: bool = False) -> dict:
    """Returns all groups from the active database."""
    # Unencrypted message:
    # {
    #     "action": "get-database-groups"
    # }
    # Response message data (success, decrypted):
    # {
    #     "defaultGroup": "<default group name>",
    #     "defaultGroupAlwaysAllow": false,
    #     "groups": [
    #         {
    #             "name": "Root",
    #             "uuid": "<group UUID>",
    #             "children": [
    #                 {
    #                     "name": "KeePassXC-Browser Passwords",
    #                     "uuid": "<group UUID>",
    #                     "children": []
    #                 },
    #                 ...
    #             ]
    #         }
    #     ]
    # }
    logger.debug("Query database for all groups")
    msg = {
        'action': 'get-database-groups'
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response['groups']['groups'][0]


def get_databasehash(conn: CryptConnection,
                     client_privkey: PrivateKey,
                     server_pubkey: PublicKey,
                     nonce: bytes,
                     unlock: bool = False) -> str:
    """Request for receiving the database hash (SHA256) of the current active database."""
    # Unencrypted message:
    # {
    #     "action": "get-databasehash"
    # }
    # Response message data (success, decrypted):
    # {
    #     "action": "hash",
    #     "hash": "29234e32274a32276e25666a42",
    #     "version": "2.2.0"
    # }
    msg = {'action': 'get-databasehash'}
    logger.debug("Request database hash with nonce %s", Base64Encoder.encode(nonce))
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response['hash']


def get_logins(conn: CryptConnection,
               client_privkey: PrivateKey,
               server_pubkey: PublicKey,
               nonce: bytes,
               url: str,
               submit_url: str = None,
               http_auth: bool = False,
               client_id: Union[str, list[str]] = None,
               identkey: Union[PublicKey, list[PublicKey]] = None,
               identpairs: list[dict] = None,
               unlock: bool = False):
    """Requests for receiving credentials for the current URL match."""
    # Unencrypted message:
    # {
    #     "action": "get-logins",
    #     "url": "<snip>",
    #     "submitUrl": "<optional>",  # is used (and compared exactly!) if url starts with 'file://'
    #     "httpAuth": "<optional>",
    #     "keys": [
    #         {
    #             "id": "<saved database identifier received from associate>",
    #             "key": "<saved identification public key>"
    #         },
    #         ...
    #     ]
    # }
    # Response message data (success, decrypted):
    # {
    #     "count": "2",
    #     "entries" : [
    #     {
    #         "login": "user1",
    #         "name": "user1",
    #         "password": "passwd1"
    #     }],
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "success": "true",
    #     "hash": "29234e32274a32276e25666a42",
    #     "version": "2.2.0"
    # }
    logger.debug("Query database for logins to '%s' (submitURL: %s, httpAuth: %s)", url, submit_url, http_auth)
    msg = {
        'action': 'get-logins',
        'url': url,
        'httpAuth': 'true' if http_auth else 'false',
    }
    if submit_url is not None:
        msg['submitUrl'] = submit_url

    if identpairs is not None:
        msg['keys'] = [{'id': x['id'], 'key': x['key']} for x in identpairs]
        # except:
        # raise ValueError("Parameter 'identpairs' must be a list of {'id': client_id, 'key': identkey}")
    elif client_id is not None and identkey is not None:
        if isinstance(client_id, str) and isinstance(identkey, PublicKey):
            msg['keys'] = [{
                'id': client_id,
                'key': identkey.encode(Base64Encoder).decode("ascii"),
            }]
        elif isinstance(client_id, list) and isinstance(identkey, list) and len(client_id) == len(identkey):
            msg['keys'] = [{'id': x[0], 'key': x[1].encode(Base64Encoder).decode("ascii")}
                           for x in list(zip(client_id, identkey))]
        else:
            raise ValueError("Either pass both a single client_id and identkey, or lists of both with same lengths")
    else:
        raise ValueError("You must either pass 'identpairs' or both 'client_id and 'identkey'. " +
                         "If both are given, the 'identpairs' value is used.")
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def get_totp(conn: CryptConnection,
             client_privkey: PrivateKey,
             server_pubkey: PublicKey,
             nonce: bytes,
             uuid: str,
             unlock: bool = False):
    """Request for receiving the current TOTP."""
    # (KeePassXC 2.6.1 and newer)
    # Request (no unencrypted message is needed):
    # {
    #     "action": "get-totp",
    #     "uuid": "<entry UUID>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "totp": "<TOTP>",
    #     "version": "2.2.0",
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    logger.debug("Get TOTP for entry %s", uuid)
    msg = {
        'action': 'get-totp',
        'uuid': uuid,
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def lock_database(conn: CryptConnection,
                  client_privkey: PrivateKey,
                  server_pubkey: PublicKey,
                  nonce: bytes):
    """Request for locking the database from client."""
    # Unencrypted message:
    # {
    #     "action": "lock-database"
    # }
    # Response message data (success always returns an error, decrypted):
    # {
    #     "action": "lock-database",
    #     "errorCode": 1,
    #     "error": "Database not opened",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    msg = {'action': 'lock-database'}
    logger.debug("Request locking active database")
    # TODO: Fix response handling. This is known to always 'fail' atm.
    # KeepassXC sends a signal "database-locked" before the response to this request
    # this signal is missing relevant fields of responses, so parsing fails
    try:
        response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=False)
    except KeyError:
        return True


def request_autotype(conn: CryptConnection,
                     client_privkey: PrivateKey,
                     server_pubkey: PublicKey,
                     nonce: bytes,
                     search: str,
                     unlock: bool = False):
    """Performs Global Auto-Type."""
    # (KeePassXC 2.7.0 and newer)
    # Request (no unencrypted message is needed):
    # {
    #     "action": "request-autotype",
    #     "search": "<base domain of URL>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "version": "2.7.0",
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q"
    # }
    logger.debug("Request autotype for url %s", search)
    msg = {
        'action': 'request-autotype',
        'search': search,
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def set_login(conn: CryptConnection,
              client_privkey: PrivateKey,
              server_pubkey: PublicKey,
              nonce: bytes,
              url: str,
              submit_url: str,
              client_id: str,
              login: str,
              password: str,
              group: str,
              group_uuid: str,
              uuid: str,
              download_favicon: bool = False,
              unlock: bool = False):
    """Add or update credentials in the database."""
    # Unencrypted message (downloadFavicon supported in KeePassXC 2.7.0 and later, but not when updating credentials):
    # {
    #     "action": "set-login",
    #     "url": "<snip>",
    #     "submitUrl": "<snip>",
    #     "id": "testclient",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "login": "user1",
    #     "password": "passwd1",
    #     "group": "<group name>",
    #     "groupUuid": "<group UUID>",
    #     "uuid": "<entry UUID>",
    #     "downloadFavicon": "true"
    # }
    # Response message data (success, decrypted):
    # {
    #     "count": null,
    #     "entries" : null,
    #     "error": "",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "success": "true",
    #     "hash": "29234e32274a32276e25666a42",
    #     "version": "2.2.0"
    # }
    logger.debug("Create or update login to '%s' (login: %s, password: %s, group %s)", url, login, password, group)
    msg = {
        'action': 'set-login',
        "url": url,
        "submitUrl": submit_url,
        'id': client_id,
        "nonce": Base64Encoder.encode(nonce).decode('ascii'),
        "login": login,
        "password": password,
        "group": group,
        "groupUuid": group_uuid,
        "uuid": uuid,
        "downloadFavicon": download_favicon,
    }
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response


def test_associate(conn: CryptConnection,
                   client_privkey: PrivateKey,
                   server_pubkey: PublicKey,
                   nonce: bytes,
                   identkey: PublicKey,
                   db_id: str,
                   unlock: bool = False) -> str:
    """Request for testing if the client has been associated with KeePassXC."""
    # Unencrypted message:
    # {
    #     "action": "test-associate",
    #     "id": "<saved database identifier received from associate>",
    #     "key": "<saved identification public key>"
    # }
    # Response message data (success, decrypted):
    # {
    #     "version": "2.7.0",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "hash": "29234e32274a32276e25666a42",
    #     "id": "testclient",
    #     "success": "true"
    # }
    msg = {
        'action': 'test-associate',
        'id': db_id,
        'key': identkey.encode(Base64Encoder).decode("ascii"),
    }
    logger.debug("Test database association for id %s and pubkey %s with nonce %s", db_id, server_pubkey,
                 Base64Encoder.encode(nonce))
    response = conn.send_encrypted(msg, client_privkey, server_pubkey, nonce, unlock=unlock)
    return response['id']


def passkeys_get():
    """Request for Passkeys authentication."""
    # (decrypted, KeePassXC 2.7.7 and newer)
    # Unencrypted message:
    # {
    #     "action": "passkeys-get",
    #     "publicKey": PublicKeyCredentialRequestOptions,
    #     "origin": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "keys: [
    #         {
    #             "id": "<saved database identifier received from associate>",
    #             "key": "<saved identification public key>"
    #         },
    #         ...
    #     ]
    # }
    # Response (error, decrypted):
    # {
    #     "version": "2.7.7",
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "response": {
    #         "errorCode": "<error code>"
    #     }
    # }
    raise NotImplementedError


def passkeys_register():
    """Request for Passkeys credential registration."""
    # (decrypted, KeePassXC 2.7.7 and newer)
    # Unencrypted message:
    #
    # {
    #     "action": "passkeys-register",
    #     "publicKey": PublicKeyCredentialCreationOptions,
    #     "origin": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "keys: [
    #         {
    #             "id": "<saved database identifier received from associate>",
    #             "key": "<saved identification public key>"
    #         },
    #         ...
    #     ]
    # }
    # Response (error, decrypted):
    # {
    #     "version": "2.7.7",
    #     "success": "true",
    #     "nonce": "tZvLrBzkQ9GxXq9PvKJj4iAnfPT0VZ3Q",
    #     "response": {
    #         "errorCode": "<error code>"
    #     }
    # }
    raise NotImplementedError
