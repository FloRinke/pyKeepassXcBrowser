import logging

# noinspection PyPackageRequirements
from nacl.encoding import Base64Encoder
# noinspection PyPackageRequirements
from nacl.public import PublicKey

from . import crypto, api, exceptions  # noqa
from .cryptconn import CryptConnection

logger = logging.getLogger(__name__)


class ApiConnection:
    def __init__(self, client_id, identkey: PublicKey, db_id: str = None, unlock: bool = False, timeout: int = 30):
        self.client_id = client_id
        self.identkey = identkey
        self.cryptconn = CryptConnection(client_id, timeout=timeout)
        self.db_id = db_id  # received on association
        self.unlock = unlock

        # generate + exchange session keys
        self.client_privkey, self.client_pubkey = crypto.generate_keypair()
        logger.debug(f"Generated client session privkey: %s", self.client_privkey.encode(Base64Encoder))
        logger.debug(f"Generated client session pubkey: %s", self.client_pubkey.encode(Base64Encoder))

        # exchange session keys
        logger.debug("Exchanging session keys with KeepassXC")
        nonce = crypto.generate_nonce()
        self.server_pubkey = api.change_public_keys(self.cryptconn, self.client_id, self.client_pubkey, nonce)
        logger.debug(f"Received server session pubkey: {self.server_pubkey.encode(Base64Encoder)}")

        if self.identkey is None:
            logger.debug("No identkey is given, not trying to establish association.")
        else:
            logger.debug("Testing database association, using db_id %s", self.db_id)

            # test db association
            nonce = crypto.generate_nonce()
            try:
                self.db_id = api.test_associate(self.cryptconn, self.client_privkey, self.server_pubkey, nonce,
                                                self.identkey, self.db_id, unlock=unlock)
                logger.debug(f"Association verified, db_id: %s", self.db_id)
            except exceptions.ConnectionFailedError:
                logger.debug(f"Association failed, ignoring")

    def get_client_id(self) -> str:
        return self.client_id

    def get_db_id(self) -> str:
        return self.db_id

    def get_databasehash(self) -> str:
        nonce = crypto.generate_nonce()
        return api.get_databasehash(self.cryptconn, self.client_privkey, self.server_pubkey, nonce)

    def test_associate(self) -> bool:
        nonce = crypto.generate_nonce()
        try:
            data = api.test_associate(self.cryptconn, self.client_privkey, self.server_pubkey, nonce,
                                      self.identkey, self.db_id)
            assert data == self.db_id
            return True
        except exceptions.ConnectionFailedError:
            return False

    def associate(self) -> str:
        nonce = crypto.generate_nonce()
        db_id = api.associate(self.cryptconn, self.client_privkey, self.server_pubkey, nonce,
                              self.client_pubkey, self.identkey)
        self.db_id = db_id
        return db_id

    def get_logins(self, url: str, http_auth: bool = False, submit_url: str = None) -> list:
        nonce = crypto.generate_nonce()
        data = api.get_logins(self.cryptconn, self.client_privkey, self.server_pubkey, nonce,
                              url, http_auth=http_auth, submit_url=submit_url,
                              client_id=self.client_id, identkey=self.identkey)
        return data['entries']

    def get_database_groups(self) -> dict:
        nonce = crypto.generate_nonce()
        data = api.get_database_groups(self.cryptconn, self.client_privkey, self.server_pubkey, nonce)
        return data

    def generate_password(self) -> str:
        nonce = crypto.generate_nonce()
        data = api.generate_password(self.cryptconn, self.client_privkey, self.server_pubkey, nonce)
        return data

    def create_new_group(self, name: str) -> str:
        nonce = crypto.generate_nonce()
        data = api.create_new_group(self.cryptconn, self.client_privkey, self.server_pubkey, nonce, name)
        return data['uuid']

    def get_totp(self, uuid: str) -> str:
        nonce = crypto.generate_nonce()
        data = api.get_totp(self.cryptconn, self.client_privkey, self.server_pubkey, nonce, uuid)
        return data['totp']

    def request_autotype(self, url: str) -> str:
        nonce = crypto.generate_nonce()
        data = api.request_autotype(self.cryptconn, self.client_privkey, self.server_pubkey, nonce, url)
        return data

    def set_login(self, url: str, submit_url: str, login: str, password: str, group: str, group_uuid: str, uuid: str,
                  download_favicon: bool = False) -> bool:
        nonce = crypto.generate_nonce()
        data = api.set_login(self.cryptconn, self.client_privkey, self.server_pubkey, nonce, url, submit_url,
                             self.client_id, login, password, group, group_uuid, uuid, download_favicon)
        if 'success' in data and data['success'] == "true":
            return True
        else:
            return False

    def lock_database(self):
        nonce = crypto.generate_nonce()
        data = api.lock_database(self.cryptconn, self.client_privkey, self.server_pubkey, nonce)
        return data
