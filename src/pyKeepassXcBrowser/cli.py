#!/usr/bin/env python3
import json
import logging
import os
import sys

import click
from tabulate import tabulate

from pyKeepassXcBrowser import exceptions, crypto
from pyKeepassXcBrowser.apiconn import ApiConnection

logger = logging.getLogger(__name__)


def _wrap_exceptions(func, *args, **kwargs):
    try:
        return func(*args, **kwargs)
    except exceptions.DatabaseLockedError:
        click.echo("Error: Database is locked, please unlock it to allow access.", err=True)
        raise click.Abort()
    except exceptions.KeepassXcCannotDecryptError:
        click.echo("Error: Keepass was unable to decrypt the message.", err=True)
        raise click.Abort()
    except exceptions.SocketDoesNotExistError:
        click.echo("Error: KeepassXC socket not found. Are you sure KeepassXC is running?", err=True)
        raise click.Abort()
    except exceptions.ConnectionFailedError:
        click.echo("Error: Connection to KeepassXC failed. Are you sure you the ID and keys are correct?", err=True)
        raise click.Abort()
    except exceptions.NonceMismatchError:
        click.echo("Error: Nonces of sent and received message did not match.", err=True)
        raise click.Abort()
    except exceptions.UnknownErrorCodeError:
        click.echo("Error: Keepass reported an unknown error code.", err=True)
        raise click.Abort()
    except exceptions.KeepassDatabaseNotOpenedError:  # errno(1): ERROR_KEEPASS_DATABASE_NOT_OPENED
        click.echo("Keepass Api error: Database not opened", err=True)
        raise click.Abort()
    except exceptions.KeepassDatabaseHashNotRecievedError:  # errno(2): ERROR_KEEPASS_DATABASE_HASH_NOT_RECEIVED
        click.echo("Keepass Api error: Database hash not available", err=True)
        raise click.Abort()
    except exceptions.KeepassClientPublicKeyNotReceivedError:  # errno(3): ERROR_KEEPASS_CLIENT_PUBLIC_KEY_NOT_RECEIVED
        click.echo("Keepass Api error: Client public key not received", err=True)
        raise click.Abort()
    except exceptions.KeepassCannotDecryptMessageError:  # errno(4): ERROR_KEEPASS_CANNOT_DECRYPT_MESSAGE
        click.echo("Keepass Api error: Cannot decrypt message", err=True)
        raise click.Abort()
    except exceptions.KeepassTimeoutOrNotConnectedError:  # errno(5): ERROR_KEEPASS_TIMEOUT_OR_NOT_CONNECTED
        click.echo("This error is not actually generated anywhere by KeepassXC (v2.7.8)", err=True)
        raise click.Abort()
    except exceptions.KeepassActionCanceledOrDeniedError:  # errno(6): ERROR_KEEPASS_ACTION_CANCELLED_OR_DENIED
        click.echo("Keepass Api error: Action cancelled or denied", err=True)
        raise click.Abort()
    except exceptions.KeepassCannotEncryptMessageError:  # errno(7): ERROR_KEEPASS_CANNOT_ENCRYPT_MESSAGE
        click.echo("Keepass Api error: Message encryption failed.", err=True)
        raise click.Abort()
    except exceptions.KeepassAssociationFailedError:  # errno(8): ERROR_KEEPASS_ASSOCIATION_FAILED
        click.echo("Keepass Api error: KeePassXC association failed, try again", err=True)
        raise click.Abort()
    except exceptions.KeepassKeyChangeFailedError:  # errno(9): ERROR_KEEPASS_KEY_CHANGE_FAILED
        click.echo("This error is not actually generated anywhere by KeepassXC (v2.7.8)", err=True)
        raise click.Abort()
    except exceptions.KeepassEncryptionKeyUnrecognizedError:  # errno(10): ERROR_KEEPASS_ENCRYPTION_KEY_UNRECOGNIZED
        click.echo("Keepass Api error: Encryption key is not recognized", err=True)
        raise click.Abort()
    except exceptions.KeepassNoSavedDatabasesFoundError:  # errno(11): ERROR_KEEPASS_NO_SAVED_DATABASES_FOUND
        click.echo("This error is not actually generated anywhere by KeepassXC (v2.7.8)", err=True)
        raise click.Abort()
    except exceptions.KeepassIncorrectActionError:  # errno(12): ERROR_KEEPASS_INCORRECT_ACTION
        click.echo("Keepass Api error: Incorrect action", err=True)
        raise click.Abort()
    except exceptions.KeepassEmptyMessageReceivedError:  # errno(13): ERROR_KEEPASS_EMPTY_MESSAGE_RECEIVED
        click.echo("Keepass Api error: Empty message received", err=True)
        raise click.Abort()
    except exceptions.KeepassNoUrlProvidedError:  # errno(14): ERROR_KEEPASS_NO_URL_PROVIDED
        click.echo("Keepass Api error: No URL provided", err=True)
        raise click.Abort()
    except exceptions.KeepassNoLoginsFoundError:  # errno(15): ERROR_KEEPASS_NO_LOGINS_FOUND
        click.echo("Keepass Api error: No logins found", err=True)
        raise click.Abort()
    except exceptions.KeepassNoGroupsFoundError:  # errno(16): ERROR_KEEPASS_NO_GROUPS_FOUND
        click.echo("Keepass Api error: No groups found", err=True)
        raise click.Abort()
    except exceptions.KeepassCannotCreateNewGroupError:  # errno(17): ERROR_KEEPASS_CANNOT_CREATE_NEW_GROUP
        click.echo("Keepass Api error: Cannot create new group", err=True)
        raise click.Abort()
    except exceptions.KeepassNoValidUuidProvidedError:  # errno(18): ERROR_KEEPASS_NO_VALID_UUID_PROVIDED
        click.echo("Keepass Api error: No valid UUID provided", err=True)
        raise click.Abort()
    except exceptions.KeepassAccessToAllEntriesDeniedError:  # errno(19): ERROR_KEEPASS_ACCESS_TO_ALL_ENTRIES_DENIED
        click.echo("Keepass Api error: Access to all entries is denied", err=True)
        raise click.Abort()
    except exceptions.PasskeysAttestationNotSupportedError:  # errno(20): ERROR_PASSKEYS_ATTESTATION_NOT_SUPPORTED
        click.echo("Passkeys Api error: Attestation not supported", err=True)
        raise click.Abort()
    except exceptions.PasskeysCredentialIsExcludedError:  # errno(21): ERROR_PASSKEYS_CREDENTIAL_IS_EXCLUDED
        click.echo("Passkeys Api error: Credential is excluded", err=True)
        raise click.Abort()
    except exceptions.PasskeysRequestCanceledError:  # errno(22): ERROR_PASSKEYS_REQUEST_CANCELED
        click.echo("Passkeys Api error: Passkeys request canceled", err=True)
        raise click.Abort()
    except exceptions.PasskeysInvalidUserVerificationError:  # errno(23): ERROR_PASSKEYS_INVALID_USER_VERIFICATION
        click.echo("Passkeys Api error: Invalid user verification", err=True)
        raise click.Abort()
    except exceptions.PasskeysEmptyPulicKeyError:  # errno(24): ERROR_PASSKEYS_EMPTY_PUBLIC_KEY
        click.echo("Passkeys Api error: Empty public key", err=True)
        raise click.Abort()
    except exceptions.PasskeysInvalidUrlProvidedError:  # errno(25): ERROR_PASSKEYS_INVALID_URL_PROVIDED
        click.echo("Passkeys Api error: Invalid URL provided", err=True)
        raise click.Abort()
    except exceptions.PasskeysOriginNotAllowedError:  # errno(26): ERROR_PASSKEYS_ORIGIN_NOT_ALLOWED
        click.echo("Passkeys Api error: Origin is empty or not allowed", err=True)
        raise click.Abort()
    except exceptions.PasskeysDomainIsNotValidError:  # errno(27): ERROR_PASSKEYS_DOMAIN_IS_NOT_VALID
        click.echo("Passkeys Api error: Effective domain is not a valid domain", err=True)
        raise click.Abort()
    except exceptions.PasskeysDomainRpidMismatchError:  # errno(28): ERROR_PASSKEYS_DOMAIN_RPID_MISMATCH
        click.echo("Passkeys Api error: Origin and RP ID do not match", err=True)
        raise click.Abort()
    except exceptions.PasskeysNoSupportedAlgorithmsError:  # errno(29): ERROR_PASSKEYS_NO_SUPPORTED_ALGORITHMS
        click.echo("Passkeys Api error: No supported algorithms were provided", err=True)
        raise click.Abort()
    except exceptions.PasskeysWaitForLifetimerError:  # errno(30): ERROR_PASSKEYS_WAIT_FOR_LIFETIMER
        click.echo("Passkeys Api error: Wait for timer to expire", err=True)
        raise click.Abort()
    except exceptions.PasskeysUnknownError:  # errno(31): ERROR_PASSKEYS_UNKNOWN_ERROR
        click.echo("Passkeys Api error: Unknown passkeys error", err=True)
        raise click.Abort()
    except exceptions.PasskeysInvalidChallengeError:  # errno(32): ERROR_PASSKEYS_INVALID_CHALLENGE
        click.echo("Passkeys Api error: Challenge is shorter than required minimum length", err=True)
        raise click.Abort()
    except exceptions.PasskeysInvalidUserIdError:  # errno(33): ERROR_PASSKEYS_INVALID_USER_ID
        click.echo("Passkeys Api error: user.id does not match the required length", err=True)
        raise click.Abort()
    except NotImplementedError:
        click.echo("Error: This functionality is not yet implemented.", err=True)
        raise click.Abort()


@click.group()
@click.option('--id', '-i', 'client_id', default="kpxcb-cli", envvar='PKPXCB_CLIENT_ID',
              help="client_id for KeepassXC-connection")
@click.option('--identkey', '-I', 'ident_key', envvar='PKPXCB_IDENTKEY',
              help="identification  key for KeepassXC-connection as base64")
@click.option('--dbid', '-d', 'db_id', default="kpxcb-cli", envvar='PKPXCB_DATABASE_ID',
              help="connectionid as received from keepass on associating")
@click.option('--log', type=click.Choice(['debug', 'info', 'warning', 'error', 'critical']),
              envvar='PKPXCB_LOG')
@click.option('--unlock', '-u', is_flag=True, envvar='PKPXCB_UNLOCK', help="Request database unlock.")
@click.pass_context
def cli(ctx, client_id, ident_key, db_id, log, unlock):
    """KeepassXC-Browser python Cli"""
    if log is not None:
        click.echo("Call params (after processing):")
        for key in tuple(vars().keys()):
            click.echo(f" - {key}: {vars()[key]} ({ctx.get_parameter_source(key)})")
        click.echo("Environment values:")
        for val in os.environ:
            if val.startswith('PKPXCB_'):
                click.echo(f" - {val}: {os.environ[val]}")
        click.echo(f"logging activate: {log}", err=True)
        if log == 'debug':
            lv = logging.DEBUG
        elif log == 'info':
            lv = logging.INFO
        elif log == 'warning':
            lv = logging.WARNING
        elif log == 'error':
            lv = logging.ERROR
        elif log == 'critical':
            lv = logging.CRITICAL
        else:
            lv = None
        logging.basicConfig(stream=sys.stderr, level=lv)
    ctx.obj = dict()
    if ident_key is not None:
        ident_key = crypto.build_sharedkey(ident_key)

    # open connection
    ctx.obj['api'] = _wrap_exceptions(ApiConnection, client_id, ident_key, db_id, unlock=unlock)


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def dbhash(ctx):
    """Get database hash"""
    logger.debug("enter dbhash")

    api: ApiConnection = ctx.obj['api']

    click.echo("Query database hash from server", err=True)
    data = _wrap_exceptions(api.get_databasehash)
    click.echo(f"Received hash: ", err=True, nl=False)
    click.echo(data)


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def force_associate(ctx):
    """Force new Association with database"""
    logger.debug("enter force associate")

    api: ApiConnection = ctx.obj['api']

    click.echo(f"Force re-associating with database", err=True)
    data = _wrap_exceptions(api.associate)
    click.echo(f"db_id: ", err=True, nl=False)
    click.echo(data)


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def test_associate(ctx):
    """Test association with database"""
    logger.debug("enter test_associate")

    api: ApiConnection = ctx.obj['api']
    click.echo(f"Test association with database", err=True)
    if _wrap_exceptions(api.test_associate):
        click.echo("Association with database is valid, nothing to do.", err=True)
    else:
        click.echo(f"Association could not be verified.", err=True)
        ctx.exit(1)


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def associate(ctx):
    """Test association with database and associate if unsuccessful"""
    logger.debug("enter associate")

    api: ApiConnection = ctx.obj['api']

    click.echo(f"Test association with database id '{api.get_db_id()}", err=True)
    if _wrap_exceptions(api.test_associate):
        click.echo("Association with database is valid, nothing to do.", err=True)
    else:
        click.echo(f"Association could not be verified. Reassociating...", err=True)
        data = _wrap_exceptions(api.associate)
        click.echo(f"Successfully associated with database, received id: ", err=True, nl=False)
        click.echo(data)


# noinspection PyUnusedLocal
@cli.command()
@click.argument('url', envvar='PKPXCB_URL')
@click.option('--http', '-h', 'http_auth', is_flag=True, envvar='PKPXCB_HTTP_AUTH',
              help="Filter for credentials to be used with http auth")
@click.option('--form-url', '-s', 'submit_url', envvar='PKPXCB_SUBMIT_URL',
              help="Additional URL checked (exactly) if url starts with 'file://'.")
@click.option('--json', '-j', 'as_json', is_flag=True, envvar='PKPXCB_JSON_OUTPUT', help="Return result as json")
@click.pass_context
def get_logins(ctx, url: str, http_auth: bool, submit_url: bool, as_json: bool):
    """Find credentials for url"""
    logger.debug("enter get_logins")
    click.echo(f"Requesting logins for '{url}'", err=True)

    api: ApiConnection = ctx.obj['api']

    logins = _wrap_exceptions(api.get_logins, url, http_auth, submit_url)

    if as_json:
        click.echo(json.dumps(logins))
    else:
        if len(logins) > 0:
            click.echo(tabulate(logins, headers='keys'))
        else:
            click.echo(f"No authorized credentials for '{url}'")


# noinspection PyUnusedLocal
@cli.command()
@click.option('--json', '-j', 'as_json', is_flag=True, envvar='PKPXCB_JSON_OUTPUT', help="Return result as json")
@click.pass_context
def get_groups(ctx, as_json: bool):
    """List all groups in database"""
    logger.debug("enter get_groups")
    click.echo(f"Fetching database groups", err=True)

    api: ApiConnection = ctx.obj['api']

    groups = _wrap_exceptions(api.get_database_groups)

    if as_json:
        click.echo(json.dumps(groups))
    else:
        click.echo("Database groups:")
        click.echo(_print_tree(groups))


def _print_tree(element, last=True, prefix='', childkey='children'):
    """Print a tree structure with some bells and whistles"""
    text = ""
    elbow = "└─"
    pipe = "│ "
    tee = "├─"
    blank = "  "
    text += prefix + (elbow if last else tee) + element['name'] + '\n'
    if childkey in element:
        for i, c in enumerate(element[childkey]):
            text += _print_tree(c, prefix=prefix + (blank if last else pipe), last=i == len(element[childkey]) - 1)
    return text


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def generate_password(ctx):
    """Generate a new password."""
    logger.debug("enter generate_password")

    api: ApiConnection = ctx.obj['api']

    data = _wrap_exceptions(api.generate_password)
    click.echo(data)


# noinspection PyUnusedLocal
@cli.command()
@click.argument('name', envvar='PKPXCB_NAME')
@click.pass_context
def create_new_group(ctx, name):
    """Generate a new password."""
    logger.debug("enter create_new_group")

    api: ApiConnection = ctx.obj['api']

    uuid = _wrap_exceptions(api.create_new_group, name)
    click.echo(f"Found or created group: {name}, UUID: ", err=True, nl=False)
    click.echo(uuid)


# noinspection PyUnusedLocal
@cli.command()
@click.argument('uuid', envvar='PKPXCB_UUID')
@click.pass_context
def get_totp(ctx, uuid):
    """Generate a new password."""
    logger.debug("enter get_totp")

    api: ApiConnection = ctx.obj['api']

    totp = _wrap_exceptions(api.get_totp, uuid)
    click.echo(f"Received TOTP for UUID {uuid}: ", err=True, nl=False)
    click.echo(totp)


# noinspection PyUnusedLocal
@cli.command()
@click.argument('url', envvar='PKPXCB_URL')
@click.pass_context
def request_autotype(ctx, url):
    """Generate a new password."""
    logger.debug("enter request_autotype")

    api: ApiConnection = ctx.obj['api']

    data = _wrap_exceptions(api.request_autotype, url)
    click.echo(f"Requested autotype for {url}: ", err=True)
    click.echo(data)


# noinspection PyUnusedLocal
@cli.command()
@click.option('--url', '-u', envvar='PKPXCB_URL', help="URL for entry.")
@click.option('--form-url', '-s', 'submit_url', envvar='PKPXCB_FORM_URL',
              help="Additional URL checked (exactly) if url starts with 'file://'.")
@click.option('--login', '-l', default="", envvar='PKPXCB_LOGIN', help="Login/Username")
@click.option('--password', '-p', default="", envvar='PKPXCB_PASSWORD', help="Password")
@click.option('--group', '-g', default="", envvar='PKPXCB_GROUP', help="Group")
@click.option('--group-uuid', default="", envvar='PKPXCB_GROUP_UUID', help="Group UUID")
@click.option('--uuid', default="", envvar='PKPXCB_UUID', help="UUID")
@click.option('--favicon', is_flag=True, envvar='PKPXCB_DOWNLOAD_FAVICON', help="Download favicon")
@click.pass_context
def set_login(ctx, url, submit_url, login, password, group, group_uuid, uuid, favicon):
    """Generate a new password."""
    logger.debug("enter set_login")

    api: ApiConnection = ctx.obj['api']

    click.echo(f"Add login for {login}/{password} to {url}: ", err=True)
    success = _wrap_exceptions(api.set_login, url, submit_url, login, password, group, group_uuid, uuid, favicon)
    if success:
        click.echo("successful")
    else:
        click.echo("failed")
        ctx.exit(1)


# noinspection PyUnusedLocal
@cli.command()
@click.pass_context
def lock_database(ctx):
    """Lock the database."""
    logger.debug("enter lock_database")

    api: ApiConnection = ctx.obj['api']

    response = _wrap_exceptions(api.lock_database)
    click.echo("Database locked")


if __name__ == '__main__':
    cli(sys.argv[1:])
