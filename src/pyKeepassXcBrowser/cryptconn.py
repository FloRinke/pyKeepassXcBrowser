import json
import logging

# noinspection PyPackageRequirements
from nacl.encoding import Base64Encoder
# noinspection PyPackageRequirements
from nacl.public import PublicKey, PrivateKey, EncryptedMessage

from . import crypto
from . import exceptions as kpxcb_exceptions  # noqa
from .sockconn import SockConnection

logger = logging.getLogger(__name__)


class CryptConnection:
    def __init__(self, client_id: str, timeout: int = 30):
        self.client_id = client_id
        self.sock = SockConnection(timeout=timeout)
        self.sock.connect()

    def get_client_id(self) -> str:
        return self.client_id

    def send_plain(self, data: dict):
        logger.debug("Send unencrypted message: %s", data)
        response = json.loads(self.sock.send(json.dumps(data)))
        logger.debug("Received unencrypted json response: %s", response)
        return response

    def send_encrypted(self, data: dict,
                       client_privkey: PrivateKey, server_pubkey: PublicKey,
                       nonce: bytes = None, unlock: bool = False):
        logger.debug("Send plain message: %s", data)

        ciphertext = crypto.encrypt(client_privkey, server_pubkey, json.dumps(data), nonce)
        cryptmessage = self._wrap_encrypted(data['action'], ciphertext, unlock=unlock)

        logger.debug("Send wrapped message: %s", cryptmessage)
        # cryptresponse = json.loads(self.sock.send(json.dumps(cryptmessage)))
        cryptmessage_raw = json.dumps(cryptmessage)
        cryptresponse_raw = self.sock.send(cryptmessage_raw)
        cryptresponse = json.loads(cryptresponse_raw)
        logger.debug("Received crypt response: %s", cryptresponse)

        self._check_errors(cryptresponse)

        nonce_received = Base64Encoder.decode(cryptresponse['nonce'])
        if not crypto.verify_nonce(nonce, nonce_received):
            raise kpxcb_exceptions.NonceMismatchError()

        plainresponse = crypto.decrypt(client_privkey, server_pubkey,
                                       cryptresponse['message'].encode('ascii'),
                                       nonce_received)
        logger.debug("Received unwrapped response: %s", plainresponse)
        responsepayload = json.loads(plainresponse)
        logger.debug("Received plain response: %s", responsepayload)
        return responsepayload

    @staticmethod
    def _check_errors(cryptresponse):
        if 'error' in cryptresponse or ("success" in cryptresponse and not cryptresponse['success']):
            if 'errorCode' in cryptresponse:
                if cryptresponse['errorCode'] == "1":
                    raise kpxcb_exceptions.DatabaseLockedError()
                elif cryptresponse['errorCode'] == "4":
                    raise kpxcb_exceptions.KeepassXcCannotDecryptError()
                elif cryptresponse['errorCode'] == "8":
                    raise kpxcb_exceptions.ConnectionFailedError()
                else:
                    raise kpxcb_exceptions.UnknownErrorCodeError()
            else:
                raise kpxcb_exceptions.UnknownErrorCodeError()

    def _wrap_encrypted(self, action: str, cryptmsg: EncryptedMessage, unlock: bool = False) -> dict:
        msg = {
            "action": action,
            "message": cryptmsg.ciphertext.decode('ascii'),
            "nonce": cryptmsg.nonce.decode('ascii'),
            "clientID": self.client_id
        }
        if unlock:
            msg['triggerUnlock'] = True
        return msg
