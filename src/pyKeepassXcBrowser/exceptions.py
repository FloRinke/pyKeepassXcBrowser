class PyKeepassXCBrowserBaseException(Exception):
    """Base Exception class for all Exceptions raised by pyKeepassXcBrowser

    Exceptions are unepected events within the program flow and can usually be handled by the program.
    """
    pass


class PyKeepassXCBrowserBaseError(PyKeepassXCBrowserBaseException):
    """Base Exception class for all Errors raised by pyKeepassXcBrowser

    Errors are serious issues which can not be handled by the programm
    """
    pass


class PyKeepassXCBrowserCryptoError(PyKeepassXCBrowserBaseException):
    """Base Exception class for all Errors related to client-side crypto operations."""
    pass


class PyKeepassXCBrowserApiError(PyKeepassXCBrowserBaseError):
    """Base Error class for all Errors occurred remotely and received in API responses."""
    pass


class NonceMismatchError(PyKeepassXCBrowserCryptoError):
    """Nonces of sent and received message did not match."""
    pass


class UnknownErrorCodeError(PyKeepassXCBrowserBaseError):
    """Received an unknown error code from KeepassXC."""
    pass


class DatabaseLockedError(PyKeepassXCBrowserBaseError):
    """Action could not be performed because the database is locked."""
    pass


class KeepassXcCannotDecryptError(PyKeepassXCBrowserBaseError):
    """KeepassXC could not decrypt the message it received."""
    pass


class CannotIdentifySocketPathError(PyKeepassXCBrowserBaseError):
    """There is no way to automatically find the KeepassXC socket, please specify it."""
    pass


class SocketDoesNotExistError(PyKeepassXCBrowserBaseError):
    """Cannot open KeepassXC socket, it does not exist."""
    pass


class ConnectionFailedError(PyKeepassXCBrowserBaseError):
    """Connection to KeepassXC failed. Are you sure you supplied the correct IDs and keys?"""
    pass


class KeepassApiBaseError(PyKeepassXCBrowserApiError):
    """Base Error class for all Api errors concerning Keepass functionality."""
    pass


class PasskeysApiBaseError(PyKeepassXCBrowserApiError):
    """Base Error class for all Api errors concerning Passkeys functionality."""
    pass


# KeepassXC API Errors
class KeepassDatabaseNotOpenedError(KeepassApiBaseError):
    """Keepass Api error: Database not opened"""
    # ERROR_KEEPASS_DATABASE_NOT_OPENED = 1,
    pass


class KeepassDatabaseHashNotRecievedError(KeepassApiBaseError):
    """Keepass Api error: Database hash not available"""
    # ERROR_KEEPASS_DATABASE_HASH_NOT_RECEIVED = 2,
    pass


class KeepassClientPublicKeyNotReceivedError(KeepassApiBaseError):
    """Keepass Api error: Client public key not received"""
    # ERROR_KEEPASS_CLIENT_PUBLIC_KEY_NOT_RECEIVED = 3,
    pass


class KeepassCannotDecryptMessageError(KeepassApiBaseError):
    """Keepass Api error: Cannot decrypt message"""
    # ERROR_KEEPASS_CANNOT_DECRYPT_MESSAGE = 4,
    pass


class KeepassTimeoutOrNotConnectedError(KeepassApiBaseError):
    """This error is not actually generated anywhere by KeepassXC (v2.7.8)"""
    # ERROR_KEEPASS_TIMEOUT_OR_NOT_CONNECTED = 5,
    pass


class KeepassActionCanceledOrDeniedError(KeepassApiBaseError):
    """Keepass Api error: Action cancelled or denied"""
    # ERROR_KEEPASS_ACTION_CANCELLED_OR_DENIED = 6,
    pass


class KeepassCannotEncryptMessageError(KeepassApiBaseError):
    """Keepass Api error: Message encryption failed."""
    # ERROR_KEEPASS_CANNOT_ENCRYPT_MESSAGE = 7,
    pass


class KeepassAssociationFailedError(KeepassApiBaseError):
    """Keepass Api error: KeePassXC association failed, try again"""
    # ERROR_KEEPASS_ASSOCIATION_FAILED = 8,
    pass


class KeepassKeyChangeFailedError(KeepassApiBaseError):
    """This error is not actually generated anywhere by KeepassXC (v2.7.8)"""
    # ERROR_KEEPASS_KEY_CHANGE_FAILED = 9,
    pass


class KeepassEncryptionKeyUnrecognizedError(KeepassApiBaseError):
    """Keepass Api error: Encryption key is not recognized"""
    # ERROR_KEEPASS_ENCRYPTION_KEY_UNRECOGNIZED = 10,
    pass


class KeepassNoSavedDatabasesFoundError(KeepassApiBaseError):
    """This error is not actually generated anywhere by KeepassXC (v2.7.8)"""
    # ERROR_KEEPASS_NO_SAVED_DATABASES_FOUND = 11,
    pass


class KeepassIncorrectActionError(KeepassApiBaseError):
    """Keepass Api error: Incorrect action"""
    # ERROR_KEEPASS_INCORRECT_ACTION = 12,
    pass


class KeepassEmptyMessageReceivedError(KeepassApiBaseError):
    """Keepass Api error: Empty message received"""
    # ERROR_KEEPASS_EMPTY_MESSAGE_RECEIVED = 13,
    pass


class KeepassNoUrlProvidedError(KeepassApiBaseError):
    """Keepass Api error: No URL provided"""
    # ERROR_KEEPASS_NO_URL_PROVIDED = 14,
    pass


class KeepassNoLoginsFoundError(KeepassApiBaseError):
    """Keepass Api error: No logins found"""
    # ERROR_KEEPASS_NO_LOGINS_FOUND = 15,
    pass


class KeepassNoGroupsFoundError(KeepassApiBaseError):
    """Keepass Api error: No groups found"""
    # ERROR_KEEPASS_NO_GROUPS_FOUND = 16,
    pass


class KeepassCannotCreateNewGroupError(KeepassApiBaseError):
    """Keepass Api error: Cannot create new group"""
    # ERROR_KEEPASS_CANNOT_CREATE_NEW_GROUP = 17,
    pass


class KeepassNoValidUuidProvidedError(KeepassApiBaseError):
    """Keepass Api error: No valid UUID provided"""
    # ERROR_KEEPASS_NO_VALID_UUID_PROVIDED = 18,
    pass


class KeepassAccessToAllEntriesDeniedError(KeepassApiBaseError):
    """Keepass Api error: Access to all entries is denied"""
    # ERROR_KEEPASS_ACCESS_TO_ALL_ENTRIES_DENIED = 19,
    pass


# KeepassXC Passkeys API Errors
class PasskeysAttestationNotSupportedError(PasskeysApiBaseError):
    """Passkeys Api error: Attestation not supported"""
    # ERROR_PASSKEYS_ATTESTATION_NOT_SUPPORTED = 20,
    pass


class PasskeysCredentialIsExcludedError(PasskeysApiBaseError):
    """Passkeys Api error: Credential is excluded"""
    # ERROR_PASSKEYS_CREDENTIAL_IS_EXCLUDED = 21,
    pass


class PasskeysRequestCanceledError(PasskeysApiBaseError):
    """Passkeys Api error: Passkeys request canceled"""
    # ERROR_PASSKEYS_REQUEST_CANCELED = 22,
    pass


class PasskeysInvalidUserVerificationError(PasskeysApiBaseError):
    """Passkeys Api error: Invalid user verification"""
    # ERROR_PASSKEYS_INVALID_USER_VERIFICATION = 23,
    pass


class PasskeysEmptyPulicKeyError(PasskeysApiBaseError):
    """Passkeys Api error: Empty public key"""
    # ERROR_PASSKEYS_EMPTY_PUBLIC_KEY = 24,
    pass


class PasskeysInvalidUrlProvidedError(PasskeysApiBaseError):
    """Passkeys Api error: Invalid URL provided"""
    # ERROR_PASSKEYS_INVALID_URL_PROVIDED = 25,
    pass


class PasskeysOriginNotAllowedError(PasskeysApiBaseError):
    """Passkeys Api error: Origin is empty or not allowed"""
    # ERROR_PASSKEYS_ORIGIN_NOT_ALLOWED = 26,
    pass


class PasskeysDomainIsNotValidError(PasskeysApiBaseError):
    """Passkeys Api error: Effective domain is not a valid domain"""
    # ERROR_PASSKEYS_DOMAIN_IS_NOT_VALID = 27,
    pass


class PasskeysDomainRpidMismatchError(PasskeysApiBaseError):
    """Passkeys Api error: Origin and RP ID do not match"""
    # ERROR_PASSKEYS_DOMAIN_RPID_MISMATCH = 28,
    pass


class PasskeysNoSupportedAlgorithmsError(PasskeysApiBaseError):
    """Passkeys Api error: No supported algorithms were provided"""
    # ERROR_PASSKEYS_NO_SUPPORTED_ALGORITHMS = 29,
    pass


class PasskeysWaitForLifetimerError(PasskeysApiBaseError):
    """Passkeys Api error: Wait for timer to expire"""
    # ERROR_PASSKEYS_WAIT_FOR_LIFETIMER = 30,
    pass


class PasskeysUnknownError(PasskeysApiBaseError):
    """Passkeys Api error: Unknown passkeys error"""
    # ERROR_PASSKEYS_UNKNOWN_ERROR = 31,
    pass


class PasskeysInvalidChallengeError(PasskeysApiBaseError):
    """Passkeys Api error: Challenge is shorter than required minimum length"""
    # ERROR_PASSKEYS_INVALID_CHALLENGE = 32,
    pass


class PasskeysInvalidUserIdError(PasskeysApiBaseError):
    """Passkeys Api error: user.id does not match the required length"""
    # ERROR_PASSKEYS_INVALID_USER_ID = 33,
    pass
