import logging
import socket
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)


class BaseSocket(ABC):
    @abstractmethod
    def connect(self, address: str):
        pass

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def send(self, data: bytes):
        pass

    @abstractmethod
    def read(self, buff_size: bytes):
        pass


class PosixSocket(BaseSocket):
    def __init__(self, timeout: int, buff_size: int):
        self.sock = socket.socket(family=socket.AF_UNIX)
        # self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, buff_size)
        # self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, buff_size)
        self.sock.settimeout(timeout)

    def connect(self, address: str):
        self.sock.connect(address)
        # try:
        #    pass
        # except socket.error:
        #    self.sock.close()
        #    raise Exception("Could not connect to {addr}".format(addr=address))

    def send(self, data: bytes):
        self.sock.send(data)

    def read(self, buff_size: int):
        return self.sock.recv(buff_size)

    def close(self):
        self.sock.close()
