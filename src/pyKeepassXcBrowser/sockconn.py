import logging
import os
from pathlib import Path

from .sock import PosixSocket
from . import exceptions  # noqa

logger = logging.getLogger(__name__)

SOCKET_NAME = 'org.keepassxc.KeePassXC.BrowserServer'
BUFFSIZE = 256*1024


class SockConnection:
    def __init__(self, socket_name: str = SOCKET_NAME, timeout: int = 30):
        self.address = None
        self.sock = None
        if 'XDG_RUNTIME_DIR' in os.environ:
            xdg_runtime_dir = Path(os.getenv('XDG_RUNTIME_DIR'))
            logger.debug("Choose socket by XDG_RUNTIME_DIR: %s", xdg_runtime_dir)
            socket_path = xdg_runtime_dir / socket_name
            if socket_path.exists():
                self.address = socket_path
                logger.debug("Use socket: %s", self.address)
                self.sock = PosixSocket(timeout, 1024)
            else:
                raise exceptions.SocketDoesNotExistError
        else:
            raise exceptions.CannotIdentifySocketPathError

    def connect(self):
        self.sock.connect(str(self.address))

    def disconnect(self):
        self.sock.close()

    def send(self, data: str):
        logger.debug("Send message on sock: %s", data)
        self.sock.send(data.encode('utf-8'))
        response = self.sock.read(BUFFSIZE).decode()  # TODO: Handle receiving data from socket in a reliable way
        logger.debug("Received response on sock: %s", response)
        return response
